FROM jenkins/agent:latest
FROM node:latest

RUN node -v

# Set correct TZ
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/"${TZ}" /etc/localtime && echo "${TZ}" > /etc/timezone \
    && dpkg-reconfigure -f noninteractive tzdata
